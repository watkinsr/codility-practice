package codility

import scala.collection.JavaConverters._
import scala.math._, BigDecimal._


/* We have an array of Integers
 * walk through that array and convert the array Into a Map[Number, Occurrences]
 * where each key is the number and the value is the number of occurences
 * for example {1 -> 2, 3 -> 2, 7 -> 1}
 */

object Solution {
  def solution(a: Array[Int]): Int = {
    val res = a.foldLeft(Map[Int, Int]())((a: Map[Int, Int], b: Int) => {
      val newOccurrenceValue = a.getOrElse(b, 0) + 1
      a.+((b, newOccurrenceValue))
      // a
    })
    val onlyOneOccurrence = res.filter((p: (Int, Int)) => p._2 % 2 == 1).head._1
    // prIntln(s"res: $res, onlyOneOccurrence: $onlyOneOccurrence")
    onlyOneOccurrence
  }

  def shiftOnce(a: Array[Int]): Array[Int] = {
    val restElements = a.take(a.length - 1)
    val newFirstElement = a(a.length - 1)
    Seq(Array(newFirstElement), restElements).flatten.toArray
  }

      /* The goal is to rotate array A K times; that is,
   *  each element of A will be shifted to the right K times.
   */
  def solution2(a: Array[Int], k: Int): Array[Int] = {
    def loop(a: Array[Int], k: Int): Array[Int] = {
      if (k != 0) loop(shiftOnce(a), k - 1)
      else a
    }
    loop(a, k)
  }
  // (60, 500, 40)
  // 500/60 => 8.x
  def solution3(x: Int, y: Int, d: Int): Int = {
    // def loop(x: Int, y: Int, d: Int, count: Int): Int = {
    //   if (x >= y) count
    //   else loop(x + d, y, d, count + 1)
    // }
    val amountToJump: Int = y - x
    val jumpsRequiredFloat = BigDecimal(amountToJump)/ BigDecimal(d)
    val jumpRequired = jumpsRequiredFloat.setScale(0, RoundingMode.CEILING).toInt
    println(s"jumpsRequiredFloat: $jumpsRequiredFloat")
    jumpRequired
    // println(s"amountToJump: $amountToJump, jumpRequired: $jumpRequired, amountToJump / d = ${amountToJump.toFloat/d}")
    // jumpRequired.toInt
    // loop(x, y, d, 0)
    // if (x >= y) x
    // else solution3(x + d, y, d)
      // d
  }

  def solution4(a: Array[Int]): Int = {
    val N = a.length
    val sum1 = a.reduce(_ + _)
    val sum2 = (1 to N + 1).reduce(_ + _)
    sum2 - sum1
  }

  def solution5(a: Array[Int]): Int = {
    var (left, right) = (0, 0)
    for (i <- 1 until a.length) right += a(i)
    left += a(0)
    var ans = abs(left - right)
    for(p <- 1 until a.length) {
      val diff = abs(left - right)
      if (diff < ans) {
        ans = diff
      }
      left += a(p)
      right -= a(p)
    }
    ans
  }

  def solution6(a: Array[Int]): Int = {
    val N = a.length
    var ans = 1
    var emptyMap = Map[Int, Int]()
    for (i <- 0 until N) {
      println(s"emptyMap: $emptyMap")
      val newValue = emptyMap.getOrElse(a(i), 0) + 1
      if (newValue > 1 || a(i) > N) {
        ans = 0
      }
      emptyMap = emptyMap.+((a(i), newValue))
    }
    ans
  }

  def solution7(x: Int, a: Array[Int]): Int = {
    var leavesSoFar = Set[Int]()
    var canFrogJump = false
    var endOfSeq = false
    var ans = -1
    var c = 0
    println(s"x: $x, a.length: ${a.length}")
    if (x > a.length) {
      -1
    }
    else {
      while(!canFrogJump && !endOfSeq) {
        val leaf = a(c)
        if (leaf <= x) {
          leavesSoFar += leaf
        }
        // println(s"leavesSoFar: $leavesSoFar, c: $c")
        if (leavesSoFar.size == x) {
          canFrogJump = true
          ans = c
        }
        if (c == a.length - 1) {
          endOfSeq = true
        }
        else c += 1
      }
      ans
    }
  }

  def solution8(n: Int, a: Array[Int]): Array[Int] = {
    var counters = Array.fill(n){0}
    var currentMax = 0
    var currentMin = 0
    // println(s"counters: ${counters.toList}")
    for (i <- 0 until a.length) {
      val c = a(i)
      if (c >= 1 && c <= n) {
        if (counters(c - 1) < currentMin) {
          counters(c - 1) = currentMin + 1
        } else {
          counters(c - 1) += 1
        }
        if (counters(c - 1) > currentMax) {
          currentMax = counters(c - 1)
        }
      } else {
        currentMin = currentMax
        // for (i <- 0 until counters.length) {
        //   counters(i) = currentMax
        // }
      }
      // println(s"c: $c, counters: ${counters.toList}")
    }
    for (i <- 0 until counters.length) {
      if (counters(i) < currentMin) {
        counters(i) = currentMin
      }
    }
    counters
  }

  def solution9b(a: Array[Int]): Int = {
    val sortedArray = a.sorted
    if (a.length < 0) 1
    if (a.length == 1 && a(0) > 1) 1

    def loop(c: Int, prevValue: Int): Int = {
      val curr = sortedArray(c)
      val endOfArrayPred = c == a.length - 1
      if ((curr - prevValue > 1) && prevValue >= 0) prevValue + 1
      else if (endOfArrayPred && curr > 1) sortedArray(c) + 1
      else if (endOfArrayPred) 1
      else loop(c + 1, curr)
    }
    loop(0, sortedArray(0))
  }

  def solution9(a: Array[Int]): Int = {
    val sortedSet = collection.SortedSet(a: _*)
    val minVal = sortedSet.head
    val maxVal = sortedSet.max
    if (!(a.length > 0) || minVal > 1 || maxVal < 1) {
      1
    } else {
        val r = minVal to maxVal
        val rangeSet = collection.SortedSet(minVal to maxVal: _*)
        // r.toSet
        val diff = rangeSet diff sortedSet
        // println(s"rangeSet: $rangeSet, sortedInputSet: $sortedInputSet")
        // println(s"r: $r")
        // println(s"minVal: $minVal, maxVal: $maxVal")
        // println(s"res: $res")
        if (diff.size == 0) {
           maxVal + 1
        } else {
          if (diff.head < 1) {
            val sortedMissingNumbers = diff.toArray
            var smallestPosValue = Int.MaxValue
            for (i <- 0 until diff.size) {
              val v = sortedMissingNumbers(i)
              if (v < smallestPosValue && v >= 1) smallestPosValue = v
            }
            smallestPosValue
          }
          else diff.head
        }
    }
  }
}


object MissingInteger {
  // 100%
  def solution(A: Array[Int]): Int = {
    def findMissing(in: Int, l: List[Int]): Int = {
    if (l.isEmpty || l.head != in) in
    else findMissing(in + 1, l.tail)
    }
    findMissing(1, A.toList.filter(_ > 0).distinct.sorted)
  }

  solution(Array(1,3,6,4,1,2))
  solution(Array(1))
  solution(Array(3))
  solution(Array(0))
  solution(Array(-2))
  solution(Array(-5, 0, -2, 1))
  solution(Array(-2147483648, 2147483647))
}

object PassingCars {
  def solution(A: Array[Int]): Int = {
    def loop(eastCount: Int, carAmount: Int, c: Int): Int = {
      if (c == A.length) carAmount
      else {
        if (A(c) == 0) loop(eastCount + 1, carAmount, c + 1)
        else loop(eastCount, carAmount + eastCount, c + 1)
      }
    }
    val amount = loop(0, 0, 0)
    if (amount > 1E9 || amount < 0) return -1
    else return amount
  }
}

object GenomicRangeQuery {
  def solution(s: String, p: Array[Int], q: Array[Int]): Array[Int] = {
    Array(1)
  }
}

